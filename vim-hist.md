# Linha do tempo

- **1971:** Ken Thompson escreve o editor `ed`.

  - Desenvolvido em um PDP 11/20;
  - Inspirado no editor `qed`;
  - Trouxe a primeira implementação de expressões regulares.
  - Na época, terminais eram teleimpressoras (teletipos/TTY).
  - O comando `g/re/p` deu origem ao programa `grep`.
  - Salva com `w` e sai com `q`.

- **1975:** George Coulouris cria o editor `em` para aproveitar o advento de terminais com vídeo.

- **1976:** Bill Joy, escreve o editor `ex` (*EXtended editor*).

  - Reimplementação do `em` com otimizações de processamento.
  - Introduz o dois pontos (`:`) para entrar com comandos.
  - Novas sintaxes para uso com expressões regulares (`%s`).
  - Comando abreviações (`:ab`) para autotexto.
  - Comando `:map`, para atribuição de atalhos de teclado.
  - Implementa marcadores de linhas (`:k<caractere>`/`'<caractere>`).
  - Introduz o modo de visualização em tela cheia com o comando `:vi`.
  - No modo `vi`, utiliza as teclas `j` e `k` para navegar entre linhas.
  - Ainda no modo `vi`, utiliza as teclas `CR` e `ESC` para entrar e sair do modo de inserção de texto.

- **1978:** Na versão 2.0 do `ex`, um *link* de nome `vi` invocava o editor automaticamente no modo `vi`.

- **1979:** Bill Joy abandona o desenvolvimento do `vi` e o desenvolvimento é assumido por Mary Ann Horton.

  - Adicionado suporte a teclas de função e de direção.
  - Aprimorou a performance trocando a biblioteca de integração com o terminal `termcap` por `terminfo`.

- **1990:** Steve Kirkendall publica um clone do `vi`, o `elvis`.

- **1991:** Com base em outro clone do `vi`, o `stevie`, Bram Moolenaar lança o editor `vim` para o Amiga.

  - No lançamento, `vim` era um acrônimo para *VI iMitation*.
  - O acrônimo mudou para *VI iMproved* em 1993.
  - Trouxe a maior parte das funcionalidades presentes até hoje no `vim`.

- **1994:** Com base no `elvis`, Keith Bostic escreve o editor `nvi` para o 4.4BSD-Lite.

  - Até o momento, é a implementação do `vi` distribuída nos \*BSD.

- **2002:** A AT&T libera o código do `vi` para uso comercial.


